# gulp用テンプレート


## editorconfigについて

コードの統一性を高めるためにeditorconfigを使用します。


## css/sassについて

sass(scss記法)を用います。
src/sass/ フォルダ内のstyle.scssが最終的な出力ファイルとなります。
ページ毎にcssファイルを制作した際には@importでstyle.scssにインポートしてください。


## mixinの使い方について

現在mixinは使用していません。


## 使い方が分からない！

12grid 吉舌まで！


## 変更履歴
* 2018/02/01 制作
