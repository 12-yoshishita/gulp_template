// gulpを読み込む
var gulp = require('gulp');
// gulpプラグインを読み込む
var gutil = require('gulp-util');
var ejs = require("gulp-ejs"); //ejsファイルをhtmlにコンパイルするプラグイン
var concat = require('gulp-concat'); //jsファイルを結合するプラグイン
var uglify = require('gulp-uglify'); //jsファイルをmin化するプラグイン
var header = require('gulp-header'); //ファイルの先頭にライセンス情報を入力してくれるプラグイン
var plumber = require('gulp-plumber'); //エラーが出ても実行を止めないプラグイン
var sass = require('gulp-sass'); //sassをコンパイルするプラグイン
var cssmin = require('gulp-cssmin'); //cssを圧縮するプラグイン
var htmlhint = require('gulp-htmlhint'); //htmlファイルのバリデーションチェックをするプラグイン
var csslint = require('gulp-csslint'); //cssファイルのlintチェックをするプラグイン
var imagemin = require("gulp-imagemin"); //画像を圧縮するプラグインの読み込み
var pngquant = require("imagemin-pngquant"); //画像を圧縮するプラグインの読み込み
var rename = require("gulp-rename"); //ファイルを出力する際に拡張子をリネームする
var ftp = require('vinyl-ftp'); //ftp用プラグイン
var browserSync = require('browser-sync'); //ブラウザシンクするプラグイン

// ライセンス用の情報を取得するためにpackage.jsonを読み込む
var pkg = require('./package.json');
// ライセンス情報を生成する
var banner = ['/**',
  ' * <%= pkg.name %> - <%= pkg.description %>',
  ' * @version v<%= pkg.version %>',
  ' * @link <%= pkg.homepage %>',
  ' * @license <%= pkg.license %>',
  ' */',
  ''].join('\n');


// jsタスクを定義する
gulp.task('js', function () {
  // タスクを実行するグロブを指定
  gulp.src('./src/js/*.js')
    // 実行する処理を実行する順にpipeでつないでいく
    .pipe(plumber()) //エラーが出ても実行を止めない
    .pipe(concat('setting.js')) //ファイルを結合し、setting.jsファイルとして出力
    .pipe(uglify({preserveComments: 'some'})) // ファイルを圧縮する（ライセンス情報は圧縮しない）
    .pipe(header(banner, {pkg: pkg})) //ライセンス情報を挿入
    .pipe(gulp.dest('./public/js/')); //出力ディレクトリの指定
});

// sassタスクを定義
gulp.task('sass', function () {
    return gulp.src('./src/sass/**/*.scss') //読み込むscssファイルの場所を指定
    .pipe(plumber()) //エラーが出ても実行を止めない
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./src/css/')); //出力するディレクトリを指定
});

// cssタスクを定義
gulp.task('css', function() {
  gulp.src('./src/css/**/*.css')//実行する場所を指定
    .pipe(plumber()) //エラーが出ても実行を止めない
    .pipe(concat('style.css')) //ファイルを結合し、style.cssとして出力
    .pipe(gulp.dest('./src/')); //出力ディレクトリを指定
});

// cssminタスクを定義
gulp.task('cssmin', function() {
  gulp.src('./src/style.css')
    .pipe(plumber()) //エラーが出ても実行を止めない
    .pipe(cssmin())
    .pipe(gulp.dest('./public/')); //ルートディレクトリに出力
});

// htmlタスクを定義
gulp.task('html', function() {
  gulp.src('./*.html')
    .pipe(plumber()) //エラーが出ても実行を止めない
    .pipe(htmlhint())
    .pipe(htmlhint.reporter());
});

// csslintタスクを定義
gulp.task('csslint', function() {
  gulp.src('./style.css')
    .pipe(plumber()) //エラーが出ても実行を止めない
    .pipe(csslint({
      "adjoining-classes": false,
      "box-model": false,
      "box-sizing": false,
      "bulletproof-font-face": false,
      "compatible-vendor-prefixes": false,
      "empty-rules": true, //プロパティを記述していない空のセレクタをチェック
      "display-property-grouping": true, //inline要素にheight、inline-block要素にfloat、table-cell要素にmarginなど効果のない、つまり指定が誤っているプロパティをチェック
      "duplicate-background-images": false,
      "duplicate-properties": true, //同一セレクタ内で2度以上同じプロパティを指定している箇所をチェック
      "fallback-colors": false,
      "floats": false,
      "font-faces": true, //大量のweb fontのダウンロードはネットワーク負荷になるので5つ以上のweb fontを読み込んだ場合に注意喚起する
      "font-sizes": false,
      "gradients": true, //gradientプロパティを使用する場合にベンダープレフィックスが不完全でないかチェックする
      "ids": false,
      "import": true, //importで他の外部CSSを読み込んでいる場合に注意喚起する
      "important": false,
      "known-properties": true, //存在しないプロパティを記述していないか、つまり誤りがないかチェック
      "outline-none": false,
      "overqualified-elements": false,
      "qualified-headings": false,
      "regex-selectors": false,
      "shorthand": true, //margin、paddingの4方向のプロパティをまとめてではなく、各々で分けて指定している場合に注意喚起する。
      "star-property-hack": true, //IE8など古いブラウザ対策にハックを使用していないかチェック
      "text-indent": false,
      "underscore-property-hack": true, //IE7など古いブラウザ対策にハックを使用していないかチェック
      "unique-headings": false,
      "universal-selector": false,
      "unqualified-attributes": false,
      "vendor-prefix": false,
      "zero-units": true //「0px」や「0%」など単位を含む記述にゼロを指定していないかチェック
    }))
    .pipe(csslint.reporter());
});

// imgminタスクの定義
gulp.task("imagemin", function() {  // 「imageMinTask」という名前のタスクを登録
  gulp.src("./src/images/**/*.+(jpg|jpeg|png|gif|svg)")    // imagesフォルダ以下のpng画像を取得
    .pipe(imagemin({
      progressive: true,
      use: [pngquant({quality: '65-80',speed: 1})]
    }))   // 画像の圧縮処理を実行
    .pipe(gulp.dest("./images/"));    // imagesフォルダ以下に保存
});

// ejsタスクの定義
gulp.task("ejs", function() {
  gulp.src(
    ["./src/ejs/**/*.ejs",'!' + "./src/ejs/**/_*.ejs"] //対象ディレクトリ
  )
  .pipe(ejs())
  .pipe(rename({extname: ".html"})) //拡張子をhtmlに変更
  .pipe(gulp.dest("./public/")) //出力先のディレクトリ
});

// gulpタスクの定義
gulp.task('upload', function(){
  var conn = ftp.create( {
    // FTP情報を入力
    host: "sv2004.xserver.jp",
    user: "t.yoshishita@thinking-reed.com",
    pass: "8710151542626t",
    parallel: 5,
    log:      gutil.log
  });
  // ローカルのパス
  var globs = [
    './public/**/*',
  ];
  return gulp.src(globs)
  // リモートのパス
  // 指定のディレクトリにあるファイルより新しければアップロード
  // .pipe(conn.newer('/wp-content/themes/子テーマ名/test'))
  // 出力するディレクトリ
  .pipe(conn.dest('/sincero.jp/public_html/doc/'));
});

// browserSyncタスクを定義
gulp.task('browserSync', function() {
  browserSync({
    server: {
      baseDir: 'public' // ルートとなるディレクトリを指定
      // ,index: 'index.html' // インデックスファイルの指定
    }
  });
});

// liveReloadタスクを定義
gulp.task('liveReload', function() {
    browserSync.reload();
});



// startタスクを定義
gulp.task('start', ['js','sass','css','cssmin','imagemin']);

// watchタスクを定義
gulp.task('watch', function() {
  // 監視するファイルと、実行したいタスク名を指定
  gulp.watch('./src/ejs/**/*.ejs', ['ejs']);
  gulp.watch('./src/js/*.js', ['js']);
  gulp.watch('./src/sass/**/*.scss', ['sass']);
  gulp.watch('./src/css/**/*.css', ['css']);
  gulp.watch('./src/style.css', ['cssmin']);
  gulp.watch('./src/images/**/*.+(jpg|jpeg|png|gif|svg)', ['imagemin']);
  // gulp.watch('./public/**/*', ['upload']);
  gulp.watch('./public/**/*', ['liveReload']);
});

// watchタスクをdefaultタスクとして登録
gulp.task('default', ['watch', 'browserSync']);
